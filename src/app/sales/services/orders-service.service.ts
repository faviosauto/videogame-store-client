import { Injectable } from '@angular/core';
import { Order } from '../../models/order.model';
import { OrderDataStoreService } from './order-datastore.service';

@Injectable({
  providedIn: 'root',
})
export class OrdersService {
  currentOrder: Order = {
    orderDate: new Date(),
    orderDetails: [],
    totalAmount: 0,
  };

  constructor(private orderDataStoreService: OrderDataStoreService) {}

  getCurrentOrder() {
    return this.currentOrder;
  }

  createOrderDetails(orderDetails) {
    this.currentOrder.orderDetails = [
      ...this.currentOrder.orderDetails,
      orderDetails,
    ];
  }

  placeOrder(currentOrder) {
    return this.orderDataStoreService.postOrder(currentOrder);
  }

  updateTotalAmount(newTotal) {
    this.currentOrder.totalAmount += newTotal;
  }
}
