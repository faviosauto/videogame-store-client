import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Order } from 'src/app/models/order.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class OrderDataStoreService {
  today = new Date();

  currentOrder: Order = {
    orderDate: this.today,
    orderDetails: [],
    totalAmount: 0,
  };

  readonly ROOT_URL = `${environment.API_URL}/orders`;

  constructor(private http: HttpClient) {}

  getOrder() {
    return this.currentOrder;
  }

  postOrder(order) {
    return this.http.post<Order>(this.ROOT_URL, order, {
      headers: {
        'Content-Type': 'application/json',
      },
    });
  }
}
