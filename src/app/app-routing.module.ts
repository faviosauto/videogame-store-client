import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VideogameDetailsComponent } from './components/videogame-details/videogame-details.component';
import { HomeComponent } from './components/home/home.component';
import { AddGameComponent } from './components/add-game/add-game.component';
import { SeeOrderComponent } from './components/see-order/see-order.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'videogame/:id',
    component: VideogameDetailsComponent,
  },
  {
    path: 'add-game',
    component: AddGameComponent,
  },
  {
    path: 'cart',
    component: SeeOrderComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
