import { Component } from '@angular/core';
import { VideoGameDataStoreService } from './video-games/services/video-game-datastore.service';
import { VideoGame } from './models/video-game.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'GamePlay Store';
  videogames: VideoGame[];
}
