import { Component, OnInit } from '@angular/core';
import { OrderDataStoreService } from '../../sales/services/order-datastore.service';
import { OrdersService } from 'src/app/sales/services/orders-service.service';
import { Order } from 'src/app/models/order.model';
import { VideoGameService } from 'src/app/video-games/services/video-game-service.service';
import { VideoGame } from 'src/app/models/video-game.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-see-order',
  templateUrl: './see-order.component.html',
  styleUrls: ['./see-order.component.scss'],
})
export class SeeOrderComponent implements OnInit {
  videogame: VideoGame;
  currentOrder: Order;

  constructor(
    private ordersService: OrdersService,
    private router: Router,
    private orderDataStoreService: OrderDataStoreService,
    private videogamesService: VideoGameService
  ) {}

  ngOnInit(): void {
    this.currentOrder = this.ordersService.getCurrentOrder();
  }

  placeOrder(currentOrder) {
    this.ordersService.placeOrder(currentOrder).subscribe();
    this.router.navigate(['']);
  }
}
