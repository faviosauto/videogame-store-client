import { Component, OnInit } from '@angular/core';
import { VideoGame } from '../../models/video-game.model';
import { VideoGameService } from 'src/app/video-games/services/video-game-service.service';
import { OrdersService } from 'src/app/sales/services/orders-service.service';
import { Order } from 'src/app/models/order.model';
import { HttpParams } from '@angular/common/http';
import { Params, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  videogames: VideoGame[];
  order: Order;

  constructor(
    private route: ActivatedRoute,
    private videogamesService: VideoGameService,
    private ordersService: OrdersService
  ) {}

  ngOnInit(): void {
    let queryParams = this.route.snapshot.queryParamMap.get('search') || '';

    // this.fetchVideogames();
    this.videogamesService
      .fetchVideogames()
      .subscribe(data => (this.videogames = data));
    this.videogamesService.getAllVideoGames().subscribe(videogames => {
      this.videogames = videogames;
    });
    this.order = this.ordersService.getCurrentOrder();
  }

  // This Add to cart is exactly the same thing than saying add to order
  addToCart(id: string) {
    let videogame;

    this.videogamesService.getVideogame(id).subscribe(data => {
      videogame = data;
      let subtotal = videogame.price;

      let newDetail = {
        videogame: videogame,
        unitaryPrice: videogame.price,
        Quantity: 1,
        Subtotal: subtotal,
      };

      this.ordersService.updateTotalAmount(videogame.price);

      let existingOrderDetail = this.order.orderDetails.find(
        orderDetail => orderDetail.videogame.id === newDetail.videogame.id
      );

      if (existingOrderDetail !== undefined) {
        existingOrderDetail.Quantity++;
        existingOrderDetail.Subtotal =
          existingOrderDetail.Quantity * existingOrderDetail.unitaryPrice;
      } else {
        this.ordersService.createOrderDetails(newDetail);
      }
    });
  }
}
