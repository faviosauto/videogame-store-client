import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';

import { OrdersService } from '../../sales/services/orders-service.service';
import { VideoGameDataStoreService } from '../../video-games/services/video-game-datastore.service';
import { Order } from '../../models/order.model';
import { OrderDetail } from '../../models/order-detail.model';

import { faShoppingCart } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @Output() searchParams = new EventEmitter();

  icons = {
    shoppingCart: faShoppingCart,
  };

  constructor(
    private ordersService: OrdersService,
    private videogameDataStore: VideoGameDataStoreService
  ) {}

  ngOnInit(): void {}

  search(searchParam) {
    this.videogameDataStore.searchVideogames(searchParam);
  }
}
