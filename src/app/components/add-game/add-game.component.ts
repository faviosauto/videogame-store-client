import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { VideoGameService } from '../../video-games/services/video-game-service.service';
import { VideoGameDataStoreService } from '../../video-games/services/video-game-datastore.service';
import { VideoGame } from 'src/app/models/video-game.model';

@Component({
  selector: 'app-add-game',
  templateUrl: './add-game.component.html',
  styleUrls: ['./add-game.component.scss'],
})
export class AddGameComponent implements OnInit {
  videogame: VideoGame;
  AddNewGameForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private videogameService: VideoGameService,
    private router: Router,
    private videogameDataStore: VideoGameDataStoreService
  ) {
    this.buildForm();
  }

  ngOnInit() {}

  saveNewGame(event: Event) {
    event.preventDefault();

    if (this.AddNewGameForm.valid) {
      let newVideogame: string = this.AddNewGameForm.value;
      this.videogameDataStore.postVideogame(newVideogame).subscribe();
      this.router.navigate(['']);
    }
  }

  private buildForm() {
    const urlRegexPattern = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/;
    const priceRegexPattern = /\d(\.\d{1,2})?/;

    this.AddNewGameForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(100)]],
      developer: ['', [Validators.maxLength(100)]],
      description: ['', [Validators.maxLength(500)]],
      price: [, [Validators.required, Validators.pattern(priceRegexPattern)]],
      imageUrl: [
        '',
        [Validators.required, Validators.pattern(urlRegexPattern)],
      ],
      stock: ['', [Validators.required]],
    });
  }
}
