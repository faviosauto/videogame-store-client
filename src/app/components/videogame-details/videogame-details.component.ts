import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { VideoGameService } from '../../video-games/services/video-game-service.service';
import { VideoGame } from 'src/app/models/video-game.model';
import { OrdersService } from 'src/app/sales/services/orders-service.service';
import { Order } from 'src/app/models/order.model';
import { ExcelDownloadService } from 'src/app/excelDownload/services/excelDownload.service';

@Component({
  selector: 'app-videogame-details',
  templateUrl: './videogame-details.component.html',
  styleUrls: ['./videogame-details.component.scss'],
})
export class VideogameDetailsComponent implements OnInit {
  videogame: VideoGame;

  order: Order;

  constructor(
    private route: ActivatedRoute,
    private videogamesService: VideoGameService,
    private ordersService: OrdersService,
    private excelDownloadService: ExcelDownloadService
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      const { id } = params;
      this.fetchVideogame(id);
      this.order = this.ordersService.getCurrentOrder();
    });
  }

  fetchVideogame(id) {
    this.videogamesService
      .getVideogame(id)
      .subscribe(data => (this.videogame = data));
  }

  // This Add to cart is exactly the same thing than saying add to order
  addToCart(id: string) {
    let videogame = this.videogame;
    let subtotal = videogame.price;

    let newDetail = {
      videogame,
      unitaryPrice: videogame.price,
      Quantity: 1,
      Subtotal: subtotal,
    };

    this.ordersService.updateTotalAmount(videogame.price);

    let existingOrderDetail = this.order.orderDetails.find(
      orderDetail => orderDetail.videogame.id === newDetail.videogame.id
    );

    if (existingOrderDetail !== undefined) {
      existingOrderDetail.Quantity++;
      existingOrderDetail.Subtotal =
        existingOrderDetail.Quantity * existingOrderDetail.unitaryPrice;
    } else {
      this.ordersService.createOrderDetails(newDetail);
    }
  }

  downloadExcel() {
    this.excelDownloadService.downloadFile();
  }
}
