import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExcelDownloadRoutingModule } from './excelDownload-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, ExcelDownloadRoutingModule],
})
export class ExcelDownloadModule {}
