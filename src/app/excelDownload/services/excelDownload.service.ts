import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ExcelDownloadService {
  downloadFile = (): void => {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', `https://localhost:5001/api/orders/download`);
    xhr.responseType = 'blob';
    xhr.setRequestHeader(
      'Content-type',
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    );
    xhr.onload = function () {
      if (xhr.status === 200) {
        let blob = new Blob([xhr.response], {
          type:
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        });
        let fileName = 'Videogames.xlsx';
        if (window.navigator.msSaveBlob) {
          window.navigator.msSaveBlob(blob, fileName);
        } else {
          let a = document.createElement('a');
          a.href = URL.createObjectURL(blob);
          a.download = fileName;
          document.body.appendChild(a);
          a.click();
          document.body.removeChild(a);
        }
      }
    };
    xhr.send();
  };
}
