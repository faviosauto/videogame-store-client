import { VideoGame } from './video-game.model';

export class OrderDetail {
  public videogame: VideoGame;
  public unitaryPrice: number;
  public Quantity: number;
  public Subtotal: number;
}
