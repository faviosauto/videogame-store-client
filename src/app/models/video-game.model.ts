export class VideoGame {
  public id: string;
  public name: string;
  public developer?: string;
  public description: string;
  public price: number;
  public imageUrl: string;
}
