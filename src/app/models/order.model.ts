import { OrderDetail } from './order-detail.model';

export class Order {
  public id?: number;
  public orderDate: Date;
  public orderDetails: OrderDetail[];
  public totalAmount: number;
}
