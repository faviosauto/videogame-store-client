import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VideoGamesRoutingModule } from './video-games-routing.module';
import { VideoGameService } from './services/video-game-service.service';
import { VideoGameDataStoreService } from './services/video-game-datastore.service';

@NgModule({
  declarations: [],
  imports: [CommonModule, VideoGamesRoutingModule],
  providers: [VideoGameService, VideoGameDataStoreService],
})
export class VideoGamesModule {}
