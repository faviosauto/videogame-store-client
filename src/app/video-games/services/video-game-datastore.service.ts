import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { VideoGame } from 'src/app/models/video-game.model';

@Injectable({
  providedIn: 'root',
})
export class VideoGameDataStoreService {
  readonly ROOT_URL = `${environment.API_URL}/videogames`;
  videogames: VideoGame[];

  @Output() search: EventEmitter<VideoGame[]> = new EventEmitter();

  constructor(private http: HttpClient) {}

  getVideogames(searchParams = '') {
    let params = new HttpParams().set('search', searchParams);

    return this.http.get<VideoGame[]>(this.ROOT_URL, { params });
  }

  searchVideogames(params) {
    let findVideogames: VideoGame[] = [];
    this.getVideogames(params).subscribe(data => {
      this.videogames = data;
      this.search.emit(this.videogames);
    });
  }

  getVideogame(id) {
    return this.http.get<VideoGame>(this.ROOT_URL + '/' + id);
  }

  postVideogame(videogame) {
    return this.http.post<VideoGame>(this.ROOT_URL, videogame, {
      headers: {
        'Content-Type': 'application/json',
      },
    });
  }
}
