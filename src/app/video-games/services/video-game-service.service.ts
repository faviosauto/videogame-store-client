import { Injectable } from '@angular/core';
import { VideoGame } from '../../models/video-game.model';
import { VideoGameDataStoreService } from './video-game-datastore.service';

@Injectable({
  providedIn: 'root',
})
export class VideoGameService {
  videogames: VideoGame[];

  constructor(private videogameDataStore: VideoGameDataStoreService) {}

  getAllVideoGames() {
    return this.videogameDataStore.search;
  }

  getVideogame(id: string) {
    return this.videogameDataStore.getVideogame(id);
  }

  createVideogame(newVideogame) {
    this.videogames = [...this.videogames, newVideogame];
    return this.videogames;
  }

  fetchVideogames(queryParams = '') {
    return this.videogameDataStore.getVideogames(queryParams);
  }
}
