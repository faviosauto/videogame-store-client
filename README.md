# VideoGameStoreClient

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.9.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
Alternatively, if you want to skip the step of navigating yourself to the mentioned url you can use `ng serve -o` to automatically open a web browser
once the project finish building.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## **Project Notes**

---

### **What is the use of the `ng add` command?**

As seen in the [Angular Documentation](angular.io/cli/ad) the `ng add` command adds support for an external library to a project. It facilitates the implementation of an external library by adding the npg package to a workspace and it configures the project in the current working directory to use that library. All of that with just one command.

It only has one argument: `<collection>`, that is the package to be added, and has five options:

- --defaults=true|false -----------> When true, disables interactive input prompts for options with a default.
- --help=true|false|json|JSON ---> Shows a help message for this command in the console, its default is false.
- --interactive=true|false --------> When false, disables interactive input prompts.
- --registry=registry -------------> The NPM registry to use.
- --verbose=true|false -----------> Display additional details about internal operations during execution, its default is false.
